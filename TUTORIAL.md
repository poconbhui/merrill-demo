MERRILL Tutorial
================



Generating a mesh
-----------------

We'll begin by making a mesh in MEshRRILL.
To make an ellipsoidal mesh, we'll use the ellipsoid generator:

    meshrrill ellipsoid 0.05 0.05 0.05 -c 0.005 -o mesh.neu

Here, we've made an ellipsoid with semi-axes of length 0.05 um, 0.05 um, and
0.05 um. This is, of course, a sphere!
Since those are *semi-axes*, that's an ellipsoid with widths of 0.1 um, 0.1 um
and 0.1 um. This is something you should be careful of!
We've also specified that it should be meshed with elements
of size 0.005 um, and output to a file named `mesh.neu` which is a Patran
Neutral file.

For more information on meshrrill, use `meshrrill --help`,
and for more information on generating ellipsoids, use
`meshrrill ellipsoid --help`.

Other values are possible for the semi-axes, but the given values give a
quick and a nice solution for the moment.
To see this mesh, try opening it in ParaView!



Running a micromagnetic model
-----------------------------

After generating the mesh, we'll use MERRILL to run a micromagnetic simulation
using our generated geometry as a magnetic particle.
Using the demo BEMScript file `demo.bsc`
(which assumes you've run the above meshing command!):

    merrill demo.bsc

In the demo.bsc file, we loaded the file mesh.neu and set the basename for
the output as "solution". This should create two files: `solution.dat` and
`solution_mult.tec`.

The `solution.dat` file is a file containing a list of coordinate and
magnetization vectors. This is useful for reading a previously found solution
into MERRILL as a starting point for another calculation.

The `solution_mult.tec` file is a TecPlot file. This file can be read by
some visualization packages. In particular, it can be read by the free and
open source ParaView package.
Download version 4.3 at http://www.paraview.org/download/ for the next section.



Visualizing Micromagnetic Solutions
-----------------------------------

We provide a quick and dirty ParaView plugin for
visualizing micromagnetic solutions:

    MERRILL_Helicity_Viewer.xml

To use it, open ParaView and open the Tools -> Manage Custom Filters...
menu. From there, choose to Import a custom filter. Navigate to the
`MERRILL_Helicity_Viewer.xml` file on your computer, and choose that.
This should create the `MERRILL Helicity Viewer` plugin. Close the menu.

As a cautionary note: this plugin is hard coded to variables provided
by MERRILL _mult.tec files.
It can be used with other inputs, but it must be provided 3 scalar arrays
named `Mx`, `My` and `Mz`.

Next, choose to open a new file. Navigate to the `solution_mult.tec` file
and select it. If given a choice of readers, choose the regular one
(ie not the Visit viewer) because it's easier for now.

With the file opened, you should see the meshed geometry on screen.
Next, click on the `solution_mult.tec` filter in the pipeline browser.
Then navigate to the Filters menu and select "Search...". In the popup
window, begin typing "MERRILL Helicity Viewer". The filter should appear
long before all of that is typed out! Press `Enter` and the filter should
be applied after the `solution_mult.tec` in the pipeline browser. If not,
you may not have had `solution_mult.tec` selected in the pipeline browser
before searching for the Helicity Viewer.

Now that coloured glyphs have appeared on screen, try sliding the
`Minimum` and `Maximum` sliders in the Properties panel that appear
when the MERRILL Helicity Viewer filter is selected.
Due to a bug somewhere, the sliders may need to be moved a little
before the actual threshold range makes itself clear. The colouring may
also need to be rescaled, particularly when new solutions are opened.
Also, the colouring may or may not automatically select the `MHelicity`
colouring. If not, you may wish to select it. Try the other colouring options
to see what effects they might have in your visualizations!
